import random

# A list is assigned to the board variable, which will consist of 9 lists containing 9 numbers
# the list will be used when iterating to print the sudoku matrix
board = []

# the function creates a list of 9 numbers needed to create one square
def square():
    square = []
    num = len(square)
    while num < 9 :
        number = str(random.randint(1, 9))
        if number not in square:
            square.append(number)
            num += 1
    return square


# the function creates 9 lists containing 9 numbers each, which are saved to the board variable
def big_board():
    for j in range(9):
        square()
        board.append(square())
    return board
            

# function prints sudoku            
def print_board():
    l = 0
    r = 0
    for m in range(3):
        for k in range(3):
            row_lines = []
            for i in range(3):
                for j in range(3):
                    row_lines.append(board[l][r])
                    r += 1
                l += 1
                r -= 3
            l -= 3
            r += 3
            row = '|'.join(row_lines)
            print(row)
        l += 3
        r = 0


# the function checks the correctness of the rows        
def verify_row():
    l = 0
    r = 0
    row = 1
    flaga = True
    for m in range(3):
        for k in range(3):
            verify_row = []
            for i in range(3):
                for j in range(3):
                    verify_row.append(board[l][r])
                    r += 1
                l += 1
                r -= 3
            num = [int(i) for i in verify_row]
            if len(num) != 9 or sum(num) != 37:
                print("The: ", row, " row condition for the sudoku game was not met")
                flaga = False
                break
            row += 1
            l -= 3
            r += 3
        l += 3
        r = 0
        if flaga == False:
            break
            
# the function checks the correctness of the column
def verify_col():
    l = 0
    r = 0
    col = 0
    flaga = True
    for m in range(3):
        for k in range(3):
            verify_col = []
            for i in range(3):
                for j in range(3):
                    verify_col.append(board[l][r])
                    r += 3
                l += 3
                r -= 9
            col += 1
            l -= 9
            r += 1
            num = [int(i) for i in verify_col]
            if len(num) != 9 or sum(num) != 37:
                print("The: " , col, "column condition for the sudoku game was not met")
                flaga = False
                break
        l += 1
        r = 0
        if flaga == False:
            break

# control function (sudoku printing, row verification, column verification)            
def menu():
    x = input("if you want to generate sudoku, enter 1, "
              "if you want to quit, enter 2: "
              )
    while True:
        if x == '1':
            print_board()
            y = input(
                      "if you want to validate the rows, enter 1, "
                      "if you want to validate the columns, enter 2: "
                      "if you want to leave the game, enter 3: "
                      )
            while True:
                if y == '1':
                    verify_row()
                    z = input("if you want to validate the columns, entter 1: "
                              "if you want to quit entter 2: ")
                    if z == '1':
                        verify_col()
                        break
                    elif z == '2':
                        print("You leave the game")
                        break
                elif y == '2':
                    verify_col()
                    z = input("if you want to validate the rows, entter 1: "
                              "if you want to quit entter 2: ")
                    if z == '1':
                        verify_row()
                        break
                    elif z == '2':
                        print("You leave the game")
                        break
                elif y == 3:
                    print("You leave the game")
                    break
            break
        elif x == '2':
            print("You leave the game")
            break
        else:
            print("Try again")
            x = input("if you want to generate sudoku, enter 1 or enter 2 if you watn to quit: ")


# function call  
square()
big_board()
menu()

